<?php

use Altra\Headers\Dto\MarketData;

return [
    'market_url' => env('MSMARKET_URL'),
    // If you want to overwrite this class you need to extend from itself
    'market_data_class' => MarketData::class,
];
