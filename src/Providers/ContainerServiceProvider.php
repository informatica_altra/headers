<?php

namespace Altra\Headers\Providers;

use Illuminate\Support\ServiceProvider;

class ContainerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/altra_headers.php' => config_path('altra_headers.php'),
        ]);

        $this->app->bind('market_data', config('altra_headers.market_data_class'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
