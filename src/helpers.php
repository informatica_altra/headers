<?php

use Altra\Headers\Dto\MarketData;

if (! function_exists('setSessionMarket')) {
    function setSessionMarket(MarketData $marketData): void
    {
        Config::set('app-market', $marketData);
    }
}

if (! function_exists('getSessionMarket')) {
    function getSessionMarket(): MarketData | null
    {
        return Config::get('app-market');
    }
}
