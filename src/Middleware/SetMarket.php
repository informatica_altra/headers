<?php

namespace Altra\Headers\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class SetMarket
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $marketIso = ($request->headers->has('app-market')) ? $request->headers->get('app-market') : 'eu';
            $response = Cache::remember('market-'.$marketIso, now()->addMonths(2), function () use ($marketIso) {
                return Http::withHeaders([
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json',
                    'apikey' => env('KONG_API_KEY'),
                ])->get(config('altra_headers.market_url').'.pv1/market/'.$marketIso)->json();
            });
            $market = app()->make('market_data',$response['body']['market']);
            setSessionMarket($market);
        } catch (\Throwable$th) {
            return response()->error($th->getMessage());
        }

        return $next($request);
    }
}
