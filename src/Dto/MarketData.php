<?php

namespace Altra\Headers\Dto;

use Altra\Dto\DataTransfer;

class MarketData extends DataTransfer
{
    public function __construct(
        public int $id,
        public string $iso,
        public string $default_locale,
        public array $social,
        public string $email_footer,
    ) {
    }

    public static function model(): string
    {
      return '';
    }  
}
